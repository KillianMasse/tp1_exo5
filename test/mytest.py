# -*- coding: utf-8 -*-
from Calculator.simplecalculator import SimpleCalculator as SimpleCalculator

"""
Created on Mon Mar 23 12:43:34 2020

@author: Killian Massé
"""

if __name__ == "__main__":
    """
    Simple test in case of main call
    """
    VAL_A = 130
    VAL_B = 5
    VAL_A_ER = 42
    VAL_B_ER = 5.3
    VAL_A_Z = 54
    VAL_B_Z = 0
    RES = SimpleCalculator()  # creation d'un objet de ma classe
    print("Test avec entiers : ")
    print(RES.somme(VAL_A, VAL_B))
    print(RES.soustraction(VAL_A, VAL_B))
    print(RES.division(VAL_A, VAL_B))
    print(RES.multiplication(VAL_A, VAL_B))
    print("Test avec décimaux : ")
    print(RES.somme(VAL_A_ER, VAL_B_ER))
    print(RES.soustraction(VAL_A_ER, VAL_B_ER))
    print(RES.division(VAL_A_ER, VAL_B_ER))
    print(RES.multiplication(VAL_A_ER, VAL_B_ER))
    print("Test division par 0 : ")
    print(RES.division(VAL_A_Z, VAL_B_Z))
